<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-13T08:02:26+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-13T08:02:26+07:00
# @Copyright: https://annaya.id




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@Home')->name('home');
Route::get('/data-tables', 'IndexController@Datatable')->name('datatable');
Route::get('/register', 'AuthController@Register')->name('register');
Route::post('/welcome', 'AuthController@Welcome')->name('welcome');
