<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\DetilUser;
use Session;

class LevelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$level)
    {
      if(Auth::guard('web')->user()->level == 'dosen'){
        if($level == DetilUser::LevelDosen() || $level == 'dosen'){
          return $next($request);
        }
        else {
          return redirect(route('Dashboard'));
        }
      }

      if($level!=Auth::guard('web')->user()->level)
      {
        return redirect(route('Dashboard'));
      }
      return $next($request);
    }
}
