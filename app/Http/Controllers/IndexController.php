<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function Home()
    {
        return view('halaman.index');
    }
    public function Datatable(){
        return view('halaman.datatable');
    }
}
