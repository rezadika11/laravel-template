<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:54:22+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:54:22+07:00
# @Copyright: https://annaya.id




namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  // protected $fillable = [
  //   'level', 'email', 'password',
  // ];
  protected $guarded = ['id_user'];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token', 'forgot_password'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  /*protected $casts = [
        'email_verified_at' => 'datetime',
    ];*/

  protected $table = "users";
  protected $primaryKey = "id_user";
  public function setAttribute($key, $value)
  {
    $isRememberTokenAttribute = $key == $this->getRememberTokenName();
    if (!$isRememberTokenAttribute) {
      parent::setAttribute($key, $value);
    }
  }
}
