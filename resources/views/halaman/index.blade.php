@extends('layouts.main')
@section('title','Halaman Form')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Form    
        
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Halaman Form</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <h1>Media Online</h1>
            <h3>Sosial Media Developer</h3>
            <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
            <h4>Benefit Join di Media Online</h4>
            <ul>
                <li>Mendapatkan motivasi dari sesama para Developer</li>
                <li>Sharing knowlenge</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
            <h4>Cara Bergabung ke Media Online</h4>
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftarkan di <a href="{{ route('register') }}">Form Sign Up</a></li>
                <li>Selesai</li>
            </ol>
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection