@extends('layouts.main')
@section('title','Halaman Register')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Form    
        
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Halaman Form</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <h3>Buat Account Baru</h3>
    <h4>Sign Up Form</h4>

    <form action="{{ route('welcome') }}" method="post">
        @csrf
        <label for="first_name">First name :</label> <br>
        <input type="text" name="first_name">
        <br><br>
        <label for="last_name">Last name :</label> <br>
        <input type="text" name="last_name">
        <br><br>
        <label for="gender">Gender :</label> <br><br>
        <input type="radio" id="male" name="jk" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="jk" value="female">
        <label for="female">Female</label><br><br>
        <label for="nationality">Nationality</label> <br><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapura">Singapura</option>
        </select>
        <br><br>
        <label for="Language">Language Spoken</label> <br><br>
        <input type="checkbox" name="fav_language">Bahasa Indonesia
        <br>
        <input type="checkbox" name="fav_language">English
        <br>
        <input type="checkbox" name="fav_language">Other
        <br><br>
        <label for="bio">Bio</label><br><br>
        <textarea name="bio" id="" cols="30" rows="8"></textarea>
        <br>
        <button type="submit" name="btn">Sign Up</button>

    </form>
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection